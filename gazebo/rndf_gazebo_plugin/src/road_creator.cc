/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "road_creator.hh"

namespace gazebo {

RoadCreator::RoadCreator() : Creator(),
  printLabels(true) {
  textCreator.Material(Creator::TEXT_MATERIAL);
  textCreator.Scale(ignition::math::Vector3d(
    Creator::LANE_NAME_SCALE,
    Creator::LANE_NAME_SCALE,
    Creator::LANE_NAME_SCALE));
}

RoadCreator::~RoadCreator() {
}

std::vector<ignition::math::Vector3d> RoadCreator::InterpolateRoad(
  const std::vector<ignition::math::Vector3d> &_points,
  const double distanceThreshold) const {
  ignition::math::Spline spline;
  spline.AutoCalculate(true);
  std::vector<ignition::math::Vector3d> newPoints;

  // Add all the control points
  for (const auto &point : _points) {
    spline.AddPoint(point);
  }

  double distance;
  for (uint i = 0; i < (_points.size()-1); i++) {
    newPoints.push_back(_points[i]);
    distance = _points[i].Distance(_points[i+1]);
    if (distance > distanceThreshold) {
      const ignition::math::Vector3d newPoint = spline.Interpolate(i, 0.5);
      newPoints.push_back(newPoint);
    }
  }
  newPoints.push_back(_points.back());

  bool distanceCheck = true;
  for (uint i = 0; i < newPoints.size()-1; i++) {
    distance = newPoints[i].Distance(newPoints[i+1]);
    if (distance > distanceThreshold) {
      distanceCheck = false;
      break;
    }
  }
  if (distanceCheck == false) {
    return InterpolateRoad(newPoints, distanceThreshold);
  }

  return newPoints;
}

void RoadCreator::Width(const double _width) {
  if (_width <= 0.0)
    this->width = Creator::DEFAULT_LANE_WIDTH;
  else
    this->width = _width;
}
void RoadCreator::Name(const std::string &_name) {
  this->name = _name;
}
void RoadCreator::Points(const std::vector<ignition::math::Vector3d> &_points) {
  this->points = _points;
}
void RoadCreator::Material(const std::string &_material) {
  this->material = _material;
}

void RoadCreator::SetMeshCreator(gazebo::MeshCreator *_meshCreator) {
  this->meshCreator = _meshCreator;
}

bool RoadCreator::Create() {
  std::vector<ignition::math::Vector3d> laneExtents;
  std::vector<ignition::math::Pose3d> poses;

  FillPoses(points, poses);
  FillTriangleStripPoints(width, poses, laneExtents);

  if (printLabels)
    CreateNames(poses);

  std::for_each(laneExtents.begin(),
    laneExtents.end(),
    [&](ignition::math::Vector3d &extent) {
      extent.Z() = Creator::DEFAULT_HEIGHT;
    });
  meshCreator->AddVertexes(laneExtents);
  return true;
}

void RoadCreator::CreateNames(
  const std::vector<ignition::math::Pose3d> &poses) {
  uint lastPoseId = 0;
  for (uint i = 1; i < poses.size(); i++) {
    const double length = poses[i].Pos().Distance(poses[lastPoseId].Pos());
    if (length > Creator::MINIMUM_LENGTH_STEPS) {
      lastPoseId = i;
      CreateName(poses[i]);
    }
  }
  if (poses.back().Pos().Distance(poses[lastPoseId].Pos()) >
      Creator::MINIMUM_LENGTH_STEPS / 2.0) {
    CreateName(poses.back());
  }
}

void RoadCreator::CreateName(const ignition::math::Pose3d &pose) {
  textCreator.Name(name + "_text");
  textCreator.Text(name);
  ignition::math::Pose3d _pose(pose);
  _pose.Pos().Z() = _pose.Pos().Z() + Creator::LANE_TEXT_Z_OFFSET;
  _pose.Rot() = ignition::math::Quaterniond(0.0, 0.0, 0.0);
  textCreator.Pose(_pose);
  textCreator.Create();
}

void RoadCreator::FillPoses(
  const std::vector<ignition::math::Vector3d> &_points,
  std::vector<ignition::math::Pose3d> &poses) const {
  if (_points.size() < 2)
    gzthrow("At least two points are needed to work. But, " +
      std::to_string(poses.size()) + " were given.");

  for (uint i = 0; i < (_points.size()-1); i++) {
    const ignition::math::Vector3d direction =
      _points[i + 1] - _points[i];
    // Calculate Euler angles and create quaterion
    const double yaw = std::atan2(direction.Y(), direction.X()) -
      ignition::math::Angle::HalfPi.Radian();
    const double roll = std::atan2(direction.Z(), direction.Y());
    const double pitch = std::atan2(direction.Z(), direction.X());
    ignition::math::Quaterniond quaternion(roll, pitch, yaw);

    ignition::math::Pose3d pose;
    pose.Pos() = _points[i];
    pose.Rot() = quaternion;
    poses.push_back(pose);
  }
  ignition::math::Pose3d pose;
  pose.Pos() = _points.back();
  pose.Rot() = poses.back().Rot();
  poses.push_back(pose);
}

void RoadCreator::FillTriangleStripPoints(const double laneWidth,
  const std::vector<ignition::math::Pose3d> &poses,
  std::vector<ignition::math::Vector3d> &trianglePoints) {
  if (poses.size() < 2)
    gzthrow("At least two points are needed to work. But, " +
      std::to_string(poses.size()) + " were given.");

  for (uint i = 0; i < (poses.size() - 1); i++) {
    FillSegmentTriangles(laneWidth,
      poses[i],
      poses[i + 1],
      trianglePoints);
    FillSegmentConnectionTriangles(laneWidth,
      poses[i],
      poses[i + 1],
      trianglePoints);
  }
}

void RoadCreator::FillSegmentTriangles(
  const double laneWidth,
  const ignition::math::Pose3d &pose,
  const ignition::math::Pose3d &nextPose,
  std::vector<ignition::math::Vector3d> &trianglePoints) {
    std::vector<ignition::math::Vector3d> vertexes;
    vertexes.push_back(ignition::math::Vector3d());
    vertexes.push_back(ignition::math::Vector3d());
    vertexes.push_back(ignition::math::Vector3d());
    vertexes.push_back(ignition::math::Vector3d());

    ComputeExtents(pose.Pos(),
      pose.Rot().Yaw(),
      laneWidth,
      vertexes[0],
      vertexes[1]);
    ComputeExtents(nextPose.Pos(),
      pose.Rot().Yaw(),
      laneWidth,
      vertexes[2],
      vertexes[3]);

    gazebo::OrderPolar(vertexes);
    auto &a1 = vertexes[0];
    auto &a2 = vertexes[1];
    auto &b2 = vertexes[2];
    auto &b1 = vertexes[3];

    std::vector<ignition::math::Vector3d> _points;
    _points.push_back(a1);
    _points.push_back(b2);
    _points.push_back(a2);
    gazebo::OrderPolar(_points);
    trianglePoints.insert(trianglePoints.end(),
      _points.begin(), _points.end());

    _points.clear();
    _points.push_back(a1);
    _points.push_back(b1);
    _points.push_back(b2);
    gazebo::OrderPolar(_points);
    trianglePoints.insert(trianglePoints.end(),
      _points.begin(), _points.end());
}

void RoadCreator::FillSegmentConnectionTriangles(
  const double laneWidth,
  const ignition::math::Pose3d &pose,
  const ignition::math::Pose3d &nextPose,
  std::vector<ignition::math::Vector3d> &trianglePoints) {
    std::vector<ignition::math::Vector3d> vertices;
    vertices.push_back(ignition::math::Vector3d());
    vertices.push_back(ignition::math::Vector3d());
    vertices.push_back(ignition::math::Vector3d());
    vertices.push_back(ignition::math::Vector3d());

    ComputeExtents(nextPose.Pos(),
      pose.Rot().Yaw(),
      laneWidth,
      vertices[0],
      vertices[1]);
    ComputeExtents(nextPose.Pos(),
      nextPose.Rot().Yaw(),
      laneWidth,
      vertices[2],
      vertices[3]);
    gazebo::OrderPolar(vertices);

    std::vector<ignition::math::Vector3d> _points;
    for (uint j = 0; j < vertices.size(); j++) {
      _points.clear();
      _points.push_back(vertices[j]);
      _points.push_back(vertices[(j + 1) % vertices.size()]);
      _points.push_back(nextPose.Pos());
      gazebo::OrderPolar(_points);
      trianglePoints.insert(trianglePoints.end(),
        _points.begin(), _points.end());
    }
}

void RoadCreator::DisplayNames(const bool _printLabels) {
  printLabels = _printLabels;
}

}

