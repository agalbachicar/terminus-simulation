/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "math_util.hh"

namespace gazebo {

ignition::math::Vector3d GetCenter(
  const std::vector<ignition::math::Vector3d> &points) {
  ignition::math::Vector3d center(0.0, 0.0, 0.0);
  std::for_each(points.begin(), points.end(),
    [&](ignition::math::Vector3d const &p) {
      center = center + p;
    });
  center = center / static_cast<double>(points.size());
  return center;
}

void OrderPolar(std::vector<ignition::math::Vector3d> &points) {
  const ignition::math::Vector3d center = GetCenter(points);
  std::sort(points.begin(), points.end(),
    gazebo::PolarSort(center));
}

void ComputeExtents(const ignition::math::Vector3d &position,
  const double angle,
  const double distance,
  ignition::math::Vector3d &a,
  ignition::math::Vector3d &b) {
  double w = distance / 2.0;
  double x = std::cos(angle) * w;
  double y = std::sin(angle) * w;

  a = position;
  a.X() = a.X() + x;
  a.Y() = a.Y() + y;
  b = position;
  b.X() = b.X() - x;
  b.Y() = b.Y() - y;
}

ignition::math::SphericalCoordinates
  GetSphericalCoordinates(const double latitude,
    const double longitude, const double elev) {
  const double latInRadians = DegreeToRadian(latitude);
  const double longInRadians = DegreeToRadian(longitude);

  return ignition::math::SphericalCoordinates(
    ignition::math::SphericalCoordinates::EARTH_WGS84,
    ignition::math::Angle(latInRadians),
    ignition::math::Angle(longInRadians),
    elev,
    ignition::math::Angle::Zero);
}

double RadianToDegree(const double angle) {
  return angle * 360.0 / (2.0 * M_PI);
}

double DegreeToRadian(const double angle) {
  return angle  * (2.0 * M_PI) / 360.0;
}

bool FloatingPointAreEqual(const double a,
  const double b,
  const double epsilon) {
  return std::fabs(a - b) < epsilon;
}

}
