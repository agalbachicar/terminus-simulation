include (${project_cmake_dir}/Utils.cmake)

set (sources
  camera_controller.cc
  creator.cc
  junction_creator.cc
  math_util.cc
  perimeter_creator.cc
  projection.cc
  rndf_gazebo_plugin.cc
  road_creator.cc
  waypoint_creator.cc
  dynamic_render.cc
  text_creator.cc
  mesh_creator.cc
)

set (gtest_sources
  rndf_gazebo_plugin_TEST.cc
  math_util_TEST.cc
  road_creator_TEST.cc
  projection_TEST.cc
)

MESSAGE(STATUS "Files: ${sources}")

find_package(gazebo REQUIRED)
find_package(ignition-math3 QUIET REQUIRED)
find_package(ignition-rndf0 QUIET REQUIRED)
find_package(ignition-msgs0 QUIET REQUIRED)
find_package(ignition-transport3 QUIET REQUIRED)

find_library(rndf_gazebo_plugin_msgs rndf_gazebo_plugin_msgs ${CMAKE_BINARY_DIR}/msgs)
link_directories(
  ${GAZEBO_LIBRARY_DIRS}
  ${IGNITION-RNDF_LIBRARY_DIRS}
  ${IGNITION-TRANSPORT_LIBRARY_DIRS}
  ${CMAKE_BINARY_DIR}/msgs
)

include_directories(
  ${GAZEBO_INCLUDE_DIRS}
  ${IGNITION-RNDF_INCLUDE_DIRS}
  ${IGNITION-TRANSPORT_INCLUDE_DIRS}
  ${CMAKE_BINARY_DIR}/
  ${CMAKE_BINARY_DIR}/msgs
  ${CMAKE_BINARY_DIR}/test/
  ${PROJECT_SOURCE_DIR}/rndf_gazebo_plugin/include/
)

ign_build_tests(${gtest_sources})

if (UNIX)
  ign_add_library(${PROJECT_NAME_LOWER}${PROJECT_MAJOR_VERSION} ${sources})
elseif(WIN32)
  add_library(${PROJECT_NAME_LOWER}${PROJECT_MAJOR_VERSION} STATIC ${sources})
endif()

target_link_libraries(${PROJECT_NAME_LOWER}${PROJECT_MAJOR_VERSION} rndf_gazebo_plugin_msgs
  ${IGNITION-MATH_LIBRARIES} ${GAZEBO_LIBRARIES} ${IGNITION-RNDF_LIBRARIES})

if(WIN32)
  target_link_libraries(${PROJECT_NAME_LOWER}${PROJECT_MAJOR_VERSION} rndf_gazebo_plugin_msgs
    ws2_32 Iphlpapi
  )
else()
  target_link_libraries(${PROJECT_NAME_LOWER}${PROJECT_MAJOR_VERSION} rndf_gazebo_plugin_msgs
  )
endif()

ign_install_library(${PROJECT_NAME_LOWER}${PROJECT_MAJOR_VERSION})
add_dependencies(${PROJECT_NAME_LOWER}${PROJECT_MAJOR_VERSION} rndf_gazebo_plugin_msgs)
