/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include "dynamic_render.hh"
#include <cassert>

namespace gazebo {

DynamicRender::DynamicRender() {
  printWaypoints = true;
  printLanes = true;
  printPerimeter = true;
  printJunctions = true;
  printPerimeter = true;
  originIsSet = false;
  printLabels = true;
  interpolationDistance = Creator::DEFAULT_INTERPOLATION_DISTANCE;
  waypointMaterial = Creator::WAYPOINT_DEFAULT_MATERIAL;
  // See https://bitbucket.org/ekumen/terminus-simulation/issues/164
  // laneMaterial = Creator::LANE_DEFAULT_MATERIAL;
  // junctionMaterial = Creator::JUNCTION_DEFAULT_MATERIAL;
  // perimeterMaterial = Creator::PERIMETER_DEFAULT_MATERIAL;
  // Set the creators a pointer to the mesh creator.
  roadCreator.SetMeshCreator(&meshCreator);
  perimeterCreator.SetMeshCreator(&meshCreator);
  junctionCreator.SetMeshCreator(&meshCreator);
}

DynamicRender::~DynamicRender() {
  rndfInfo.reset();
  node->Fini();
  node.reset();
}

void DynamicRender::Load(gazebo::physics::WorldPtr _parent,
  sdf::ElementPtr _sdf) {
  worldPtr = _parent;

  ParseSDF(_sdf);
  LoadRNDFFile();
  PrintRNDFStats();

  std::vector<ignition::math::Vector3d> positions;
  GetAllWaypointLocations(positions);

  if (originIsSet)
    projection.GetRNDFSpaceLimits(positions, origin);
  else
    projection.GetRNDFSpaceLimits(positions);

  // Draw the lanes and waypoints and adjust the camera
  LoadZones(rndfInfo->Zones());
  LoadSegments(rndfInfo->Segments());
  cameraController.SetExtents(projection.GetMin(),
    projection.GetMax());

  // Subscribe to waypoints request messages, from where it receives
  // requests for paths
  node = transport::NodePtr(new transport::Node());
  this->node->Init();
  // Load the mesh of the road
  LoadRoadMesh();
  meshCreator.ClearVertexes();

  const std::string topicName = "~/road_waypoints_requests";
  this->waypointsRequestsSubscriber = this->node->Subscribe(
            topicName, &DynamicRender::OnWaypointsRequestsMsgs, this);
}


void DynamicRender::LoadRoadMesh() {
  meshCreator.Name("rndf_mesh");
  gazebo::common::Mesh *mesh = meshCreator.Create();
  gazebo::common::Material *material =
    new gazebo::common::Material(gazebo::common::Color::Black);
  mesh->AddMaterial(material);
  gazebo::common::MeshManager::Instance()->Export(
    mesh, std::string("/tmp/mesh"), std::string("dae"), true);

  gazebo::msgs::Model modelMsg;
  modelMsg.set_name(std::string("RNDF_Roads"));
  modelMsg.set_is_static(true);
  auto *poseMsg = modelMsg.mutable_pose();
  auto *positionMsg = poseMsg->mutable_position();
  positionMsg->set_x(0.);
  positionMsg->set_y(0.);
  positionMsg->set_z(0.);

  modelMsg.add_link();
  auto *linkMsg = modelMsg.mutable_link(0);
  linkMsg->set_name(std::string("RNDF_Roads_link"));

  auto *visualLinkMsg = linkMsg->add_visual();
  visualLinkMsg->set_name("RNDF_Roads_visual");
  auto *visualGeoMsg = visualLinkMsg->mutable_geometry();
  visualGeoMsg->set_type(
    gazebo::msgs::Geometry_Type::Geometry_Type_MESH);
  auto *meshVisualMsg = visualGeoMsg->mutable_mesh();
  meshVisualMsg->set_filename(std::string("/tmp/mesh/meshes/mesh.dae"));

  linkMsg->add_collision();
  auto *collisionMsg = linkMsg->mutable_collision(0);
  collisionMsg->set_id(0);
  collisionMsg->set_name(std::string("RNDF_Roads_collision"));
  auto *geometryMsg = collisionMsg->mutable_geometry();
  geometryMsg->set_type(gazebo::msgs::Geometry_Type::Geometry_Type_MESH);
  auto *meshMsg = geometryMsg->mutable_mesh();
  meshMsg->set_filename(std::string("/tmp/mesh/meshes/mesh.dae"));

  std::string modelSDFStr(
    "<sdf version='1.6'>"
    + msgs::ModelToSDF(modelMsg)->ToString("")
    + "</sdf>");

  msgs::Factory facMsg;
  facMsg.set_sdf(modelSDFStr);
  auto factoryPub = this->node->Advertise<msgs::Factory>("~/factory");
  factoryPub->Publish(facMsg);
}

void DynamicRender::ParseSDF(const sdf::ElementPtr _sdfPtr) {
  // This code gets the plugin node from the sdf file and
  // searches for different properties inside it that are
  // interesting for the plugin. In case there is no world or
  // plugin node an exception is raised.
  // 'rnfd' contains the path to the RNDF file. In case we cannot
  // find 'rndf' tag, an exception is raised.
  // 'lanes' contains a boolean indicating if we should draw them
  // 'waypoints' contains a boolean indicating if we should draw
  // them
  sdfPtr = _sdfPtr;
  if (sdfPtr->HasElement("rndf")) {
    filePath = sdfPtr->Get<std::string>("rndf");
  }
  else
    gzthrow("There is no rndf tag inside rndf node.");

  if (sdfPtr->HasElement("waypoints")) {
    printWaypoints = sdfPtr->Get<bool>("waypoints");
  }
  if (sdfPtr->HasElement("lanes")) {
    printLanes = sdfPtr->Get<bool>("lanes");
  }
  if (sdfPtr->HasElement("perimeter")) {
    printPerimeter = sdfPtr->Get<bool>("perimeter");
  }
  if (sdfPtr->HasElement("junctions")) {
    printJunctions = sdfPtr->Get<bool>("junctions");
  }

  // See issue https://bitbucket.org/ekumen/terminus-simulation/issues/164
  // if (sdfPtr->HasElement("waypoints_material")) {
  //   waypointMaterial =
  //     sdfPtr->Get<std::string>("waypoints_material");
  // }
  // if (sdfPtr->HasElement("lane_material")) {
  //   laneMaterial =
  //     sdfPtr->Get<std::string>("lane_material");
  // }
  // if (sdfPtr->HasElement("junction_material")) {
  //   junctionMaterial =
  //     sdfPtr->Get<std::string>("junction_material");
  // }
  // if (sdfPtr->HasElement("perimeter_material")) {
  //   perimeterMaterial =
  //     sdfPtr->Get<std::string>("perimeter_material");
  // }
  if (sdfPtr->HasElement("interpolation_distance")) {
    interpolationDistance =
      sdfPtr->Get<double>("interpolation_distance");
  }
  if (sdfPtr->HasElement("origin")) {
    ignition::math::Vector3d _origin =
      sdfPtr->Get<ignition::math::Vector3d>("origin");
    origin = gazebo::GetSphericalCoordinates(_origin.X(),
      _origin.Y(),
      _origin.Z());
    originIsSet = true;
  }
  if (sdfPtr->HasElement("print_labels")) {
    printLabels = sdfPtr->Get<bool>("print_labels");
  }
}

void DynamicRender::LoadRNDFFile() {
  std::string rndfFilePath = gazebo::common::find_file(filePath);
  if (rndfFilePath.empty()) {
    gzthrow(std::string("File [") +
      filePath +
      std::string("] does not exist."));
  }
  rndfInfo.reset(new ignition::rndf::RNDF(rndfFilePath));
  if (!rndfInfo->Valid()) {
    gzthrow(std::string("File [") +
      rndfFilePath +
      std::string("] is invalid"));
  }
}

void DynamicRender::PrintRNDFStats() const {
  // Show stats.
  gzmsg << "Name:               [" << rndfInfo->Name() << "]" << std::endl;
  if (!rndfInfo->Version().empty()) {
    gzmsg << "Version:            ["
      << rndfInfo->Version() << "]"
      << std::endl;
  }
  if (!rndfInfo->Date().empty()) {
    gzmsg << "Creation date:      ["
      << rndfInfo->Date() << "]"
      << std::endl;
  }
  gzmsg << "Number of segments: " << rndfInfo->NumSegments() << std::endl;
  gzmsg << "Number of zones:    " << rndfInfo->NumZones() << std::endl;
}

void DynamicRender::LoadSegments(
  const std::vector<ignition::rndf::Segment> &segments) {
  if (printLanes) {
    for (const auto &segment : segments) {
      LoadLanes(segment.Id(), segment.Lanes());
    }
  }
  if (printJunctions) {
    LoadJunctions();
  }
  if (printWaypoints) {
    for (const auto &segment : segments) {
      for (const auto &lane : segment.Lanes()) {
        LoadWaypoints(segment.Id(), lane.Id(), lane.Waypoints());
      }
    }
  }
}

void DynamicRender::LoadLanes(const int segmentParent,
  const std::vector<ignition::rndf::Lane> &lanes) {
  roadCreator.DisplayNames(printLabels);
  // See issue https://bitbucket.org/ekumen/terminus-simulation/issues/164
  // roadCreator.Material(laneMaterial);

  for (const auto &lane : lanes) {
    // Get the name of the lane
    std::string laneName = CreateLaneName(segmentParent, lane.Id());
    std::string laneUniqueName = std::to_string(segmentParent) + "."
      + std::to_string(lane.Id());
    // Create a the lanes in gazebo
    std::vector<ignition::math::Vector3d> roadPoints = GetLanePoints(lane);
    // Create a the lanes in gazebo
    roadCreator.Width(lane.Width());
    roadCreator.Points(roadPoints);
    roadCreator.Name(laneName);
    roadCreator.Create();
  }
}

void DynamicRender::FillLaneWaypointPositions(
  const std::string &laneName,
  const ignition::rndf::Lane &lane,
  std::vector<ignition::math::Vector3d> &waypointPositions) const {
  uint amountOfWaypoints = lane.Waypoints().size();
  for (uint i = 1; i <= amountOfWaypoints; i++) {
    auto waypointName = laneName + "." + std::to_string(i);
    ignition::rndf::UniqueId waypointId(waypointName);
    auto *waypoint = GetWaypointByUniqueId(waypointId);
    auto pose = GetWaypointPose(waypointId.X(), waypointId.Y(), *waypoint);
    waypointPositions.push_back(pose.Pos());
  }
}


void DynamicRender::FillLanePoints(
  const std::string &laneName,
  const ignition::rndf::Lane &lane,
  std::vector<ignition::math::Vector3d> &lanePoints) {
  std::vector<ignition::math::Vector3d> waypointPositions;
  FillLaneWaypointPositions(laneName, lane, waypointPositions);
  auto roadPoints = roadCreator.InterpolateRoad(waypointPositions,
    interpolationDistance);
  lanePoints.clear();
  lanePoints.insert(lanePoints.end(), roadPoints.begin(), roadPoints.end());
}

void DynamicRender::LoadWaypoints(const int segmentParent,
  const int laneParent,
  const std::vector<ignition::rndf::Waypoint> &waypoints,
  const gazebo::WayPointCreator::Type _type) {
    std::vector<ignition::math::Pose3d> waypointPoses =
      GetWaypointsPoses(waypoints);
    waypointCreator.Radius(Creator::DEFAULT_WAYPOINT_RADIUS);
    waypointCreator.Material(waypointMaterial);
    waypointCreator.SetType(_type);
    waypointCreator.DisplayNames(printLabels);

    for (uint i = 0; i < waypoints.size(); i++) {
      // Get the name of the waypoints and its position
      std::string name = CreateWaypointName(segmentParent,
        laneParent, waypoints[i].Id());
      // Load the waypoint in gazebo
      waypointCreator.Name(name);
      waypointCreator.Pose(waypointPoses[i]);
      waypointCreator.Create();
    }
}

std::vector<ignition::math::Pose3d> DynamicRender::GetWaypointsPoses(
  const std::vector<ignition::rndf::Waypoint> &waypoints) const {
    std::vector<ignition::math::Pose3d> poses;
    for (uint i = 0; i < waypoints.size()-1; i++) {
      ignition::math::Pose3d pose =
        GetWaypointPose(waypoints[i], waypoints[i+1]);
      poses.push_back(pose);
    }
    ignition::math::Pose3d pose(poses.back());
    ignition::math::Vector3d location =
      GetWaypointSphericalLocation(waypoints.back());
    pose.Pos() = projection.SphericalToGlobal(location);
    poses.push_back(pose);
    return poses;
}


ignition::math::Pose3d DynamicRender::GetWaypointPose(
  const uint segmentId,
  const uint laneId,
  const ignition::rndf::Waypoint &waypoint) const {
  ignition::rndf::Lane *lane = GetLaneByUniqueId(
    ignition::rndf::UniqueId(segmentId, laneId, waypoint.Id()));
  if (lane != NULL) {
    ignition::math::Pose3d pose;
    if (IsLastWaypoint(waypoint, *lane)) {
      ignition::rndf::Waypoint prevWaypoint = GetPreviousWaypoint(
        segmentId, laneId, waypoint);
      ignition::math::Pose3d prevPose = GetWaypointPose(
        prevWaypoint, waypoint);
      return GetLastWaypointPose(prevPose.Rot(), waypoint);
    }
    else
    {
      ignition::rndf::Waypoint nextWaypoint =
        GetNextWaypoint(segmentId, laneId, waypoint);
      return GetWaypointPose(waypoint, nextWaypoint);
    }
  }
  else
  {
    // This means the waypoint is a perimeter one.
    // ignition::math::Pose3d pose;
    const ignition::math::Vector3d location =
      GetWaypointSphericalLocation(waypoint);
    // pose.Pos() = projection.
    //   SphericalToGlobal(location);
    // pose.Rot() = ignition::math::Quaterniond(0.0, 0.0, 0.0);
    return ignition::math::Pose3d(
      projection.SphericalToGlobal(location),
      ignition::math::Quaterniond(0.0, 0.0, 0.0));
    // return pose;
  }
}

ignition::math::Pose3d DynamicRender::GetWaypointPose(
  const ignition::rndf::Waypoint &waypoint,
  const ignition::rndf::Waypoint &nextWaypoint) const {
    ignition::math::Vector3d location;

    location = GetWaypointSphericalLocation(waypoint);
    ignition::math::Vector3d waypointPosition =
      projection.SphericalToGlobal(location);

    location = GetWaypointSphericalLocation(nextWaypoint);
    ignition::math::Vector3d nextWaypointPosition =
      projection.SphericalToGlobal(location);

    ignition::math::Vector3d direction =
      nextWaypointPosition - waypointPosition;

    // Calculate the angle and vector
    // Add half pi because of the lane direction
    double yaw = std::atan2(direction.Y(), direction.X());
    ignition::math::Quaterniond quaternion(
      ignition::math::Vector3d(0.0, 0.0, 1.0), yaw);

    ignition::math::Pose3d pose;
    pose.Pos() = waypointPosition;
    pose.Rot() = quaternion;
    return pose;
}

ignition::math::Pose3d DynamicRender::GetLastWaypointPose(
  const ignition::math::Quaterniond &prevQuaternion,
  const ignition::rndf::Waypoint &waypoint) const {
  ignition::math::Pose3d pose;
  ignition::math::Vector3d location =
    GetWaypointSphericalLocation(waypoint);
  pose.Pos() = projection.SphericalToGlobal(location);
  pose.Rot() = prevQuaternion;
  return pose;
}

bool DynamicRender::IsLastWaypoint(
  const ignition::rndf::Waypoint &waypoint,
  const ignition::rndf::Lane &lane) const {
  if (static_cast<uint>(waypoint.Id()) == lane.NumWaypoints())
    return true;
  return false;
}

ignition::rndf::Waypoint DynamicRender::GetPreviousWaypoint(
  const uint segmentId,
  const uint laneId,
  const ignition::rndf::Waypoint &waypoint) const {
  ignition::rndf::UniqueId id(segmentId, laneId, waypoint.Id() - 1);
  return *GetWaypointByUniqueId(id);
}

ignition::rndf::Waypoint DynamicRender::GetNextWaypoint(
  const uint segmentId,
  const uint laneId,
  const ignition::rndf::Waypoint &waypoint) const {
  ignition::rndf::UniqueId id(segmentId, laneId, waypoint.Id() + 1);
  return *GetWaypointByUniqueId(id);
}

void DynamicRender::LoadZones(const std::vector<ignition::rndf::Zone> &zones) {
  for (const auto &zone : zones) {
    LoadPerimeter(zone.Id(), zone.Perimeter());
    LoadParkingSpots(zone.Id(), zone.Spots());
  }
}

void DynamicRender::LoadPerimeter(const int zoneId,
  const ignition::rndf::Perimeter &perimeter) {
  if (printPerimeter) {
    CreatePerimeter(perimeter.Points());
  }
  if (printWaypoints) {
    LoadWaypoints(zoneId, 0, perimeter.Points(),
      gazebo::WayPointCreator::Type::PERIMETER);
  }
}

void DynamicRender::CreatePerimeter(
  const std::vector<ignition::rndf::Waypoint> &waypoints) {
  std::vector<ignition::math::Vector3d> verteces;
  for (const auto &waypoint : waypoints) {
    ignition::math::Vector3d location = GetWaypointSphericalLocation(waypoint);
    verteces.push_back(projection.SphericalToGlobal(location));
  }
  // See https://bitbucket.org/ekumen/terminus-simulation/issues/164
  // perimeterCreator.Material(perimeterMaterial);
  perimeterCreator.Verteces(verteces);
  perimeterCreator.Create();
}

void DynamicRender::LoadParkingSpots(const int zoneId,
  const std::vector<ignition::rndf::ParkingSpot> &spots) {
  for (const auto &spot : spots) {
    LoadWaypoints(zoneId, spot.Id(), spot.Waypoints(),
      gazebo::WayPointCreator::Type::SPOT);
  }
}

void DynamicRender::LoadJunctions() {
  std::vector<gazebo::Junction> junctions;

  GenerateJunctions(junctions, rndfInfo->Segments());
  GenerateJunctions(junctions, rndfInfo->Zones());
  // See https://bitbucket.org/ekumen/terminus-simulation/issues/164
  // junctionCreator.Material(junctionMaterial);
  for (uint i = 0; i < junctions.size(); i++) {
    junctionCreator.Junction(junctions[i]);
    junctionCreator.Create();
  }
}

ignition::rndf::Waypoint* DynamicRender::GetWaypointByUniqueId(
  const ignition::rndf::UniqueId &waypointId) const {
  ignition::rndf::RNDFNode *rndfNode = rndfInfo->Info(waypointId);
  if (rndfNode != nullptr) {
    return rndfNode->Waypoint();
  }
  return nullptr;
}

void DynamicRender::OnWaypointsRequestsMsgs(WaypointsRequestPtr &_msg) {
  rndf_gazebo_plugin_msgs::msgs::WaypointsResponse response_msg;

  response_msg.set_client_name(_msg->client_name());

  gzmsg << "Received path request from " << _msg->client_name();
  gzmsg << " Waypoint requested " <<
    _msg->initial_waypoint().x() << " " <<
    _msg->initial_waypoint().y() << " " <<
    _msg->initial_waypoint().z() << std::endl;

    ignition::rndf::Lane *lane;
    const ignition::rndf::UniqueId wpId(
        _msg->initial_waypoint().x(),
        _msg->initial_waypoint().y(),
        _msg->initial_waypoint().z());
    lane = GetLaneByUniqueId(wpId);
    if (lane == nullptr) {
      gzerr << "Error. Lane " << CreateLaneName(wpId.X(), wpId.Y())
        << " has not been found.";
      return;
    }

    std::string laneUniqueName = std::to_string(wpId.X()) +
      "." + std::to_string(wpId.Y());
    std::vector<ignition::math::Vector3d> lanePoints;
    FillLanePoints(laneUniqueName, *lane, lanePoints);
    // Get all the waypoints positions and interpolated
    // points positions of the lane
    for (auto point : lanePoints) {
      auto *_point = response_msg.add_lane_points_positions();
      _point->set_x(point.X());
      _point->set_y(point.Y());
      _point->set_z(point.Z());
    }

    std::vector<ignition::math::Vector3d> waypointPositions;
    FillLaneWaypointPositions(laneUniqueName, *lane, waypointPositions);
    for (uint i = 0; i < waypointPositions.size(); i++) {
      auto* waypointId = response_msg.add_waypoints_ids();
      waypointId->set_x(_msg->initial_waypoint().x());
      waypointId->set_y(_msg->initial_waypoint().y());
      waypointId->set_z(i+1);

      auto* waypointPositionPtr = response_msg.add_waypoints_positions();
      waypointPositionPtr->set_x(waypointPositions[i].X());
      waypointPositionPtr->set_y(waypointPositions[i].Y());
      waypointPositionPtr->set_z(waypointPositions[i].Z());
    }

    // Get the exits and entries of the lane, then create vectors
    // to keep the positions of each one
    std::vector<ignition::rndf::Exit> &exits = lane->Exits();
    for (auto &exit : exits) {
      // Get next waypoint to go from this exit,
      // which it is the entry associated to this exit
      ignition::rndf::Waypoint *waypointPointer =
        GetWaypointByUniqueId(exit.EntryId());
      if (waypointPointer == nullptr)
        return;
      // Get the position of this entry and save it into the msg
      ignition::math::Vector3d entryLocation =
        GetWaypointSphericalLocation(*waypointPointer);
      ignition::math::Vector3d entryPosition =
        projection.SphericalToGlobal(entryLocation);
      ignition::msgs::Vector3d* entryPositionPtr =
        response_msg.add_next_entries_positions();
      entryPositionPtr->set_x(entryPosition[0]);
      entryPositionPtr->set_y(entryPosition[1]);
      entryPositionPtr->set_z(entryPosition[2]);
      // Save entry id into the message
      rndf_gazebo_plugin_msgs::msgs::UniqueId* entryIdPtr =
        response_msg.add_next_entries_ids();
      entryIdPtr->set_x(exit.EntryId().X());
      entryIdPtr->set_y(exit.EntryId().Y());
      entryIdPtr->set_z(exit.EntryId().Z());
      // Save exit ids
      rndf_gazebo_plugin_msgs::msgs::UniqueId* exitIdPtr =
        response_msg.add_exit_ids();
      exitIdPtr->set_x(exit.ExitId().X());
      exitIdPtr->set_y(exit.ExitId().Y());
      exitIdPtr->set_z(exit.ExitId().Z());
    }
    // Publish msg
    std::string topicName = std::string("~/") +
      _msg->client_name()  + "/rndf_path";
    gazebo::transport::PublisherPtr responsePub =
      node->Advertise<
        rndf_gazebo_plugin_msgs::msgs::WaypointsResponse>(topicName);

    responsePub->Publish(response_msg);
}

ignition::rndf::Lane* DynamicRender::GetLaneByUniqueId(
  const ignition::rndf::UniqueId &waypointId) const {
  ignition::rndf::RNDFNode *rndfNode = rndfInfo->Info(waypointId);
  if (rndfNode == nullptr)
    return nullptr;
  return rndfNode->Lane();
}

std::string DynamicRender::CreateLaneName(const int segmentParentId,
  const int laneId) const {
  return "lane_" + std::to_string(segmentParentId) +
    "_" + std::to_string(laneId);
}

void DynamicRender::GenerateJunctions(
  std::vector<gazebo::Junction> &junctions,
  const std::vector<ignition::rndf::Segment> &segments) const {
  for (const auto &segment : segments) {
    // Get the exits pairs of the segment
    const std::vector<ignition::rndf::Exit> &exits = GetSegmentExits(segment);
    FillJunctions(exits, junctions);
  }
}

void DynamicRender::GenerateJunctions(
  std::vector<gazebo::Junction> &junctions,
  const std::vector<ignition::rndf::Zone> &zones) const {
  for (const auto &zone : zones) {
    // Get the exits pairs of the zone
    const std::vector<ignition::rndf::Exit> &exits = zone.Perimeter().Exits();
    FillJunctions(exits, junctions);
  }
}

std::vector<ignition::rndf::Exit> DynamicRender::GetSegmentExits(
  const ignition::rndf::Segment &segment) const {
  std::vector<ignition::rndf::Exit> exits;
  for (const auto &lane : segment.Lanes()) {
    const std::vector<ignition::rndf::Exit> &_exits = lane.Exits();
    for (const auto &exit : _exits) {
      exits.push_back(exit);
    }
  }
  return exits;
}

void DynamicRender::FillJunctions(
  const std::vector<ignition::rndf::Exit> &exits,
  std::vector<gazebo::Junction> &junctions) const {
  for (const auto &exit : exits) {
    gazebo::LaneTermination ltExit =
      CreateLaneTermination(exit.ExitId());
    gazebo::LaneTermination ltEntry =
      CreateLaneTermination(exit.EntryId());
    // Due to perimeter waypoints, we need to make sure those waypoints have a
    // direction.
    if (ltExit.Pose().Rot() == ignition::math::Quaterniond(0.0, 0.0, 0.0) &&
        ltEntry.Pose().Rot() != ignition::math::Quaterniond(0.0, 0.0, 0.0)) {
      ltExit.Pose().Rot() = ltEntry.Pose().Rot();
    }
    if (ltEntry.Pose().Rot() == ignition::math::Quaterniond(0.0, 0.0, 0.0) &&
        ltExit.Pose().Rot() != ignition::math::Quaterniond(0.0, 0.0, 0.0)) {
      ltEntry.Pose().Rot() = ltExit.Pose().Rot();
    }

    std::vector<gazebo::LaneTermination> lts;
    lts.push_back(ltEntry);
    lts.push_back(ltExit);

    const int junctionId =
      gazebo::Junction::GetJunctionIndexById(junctions, lts);

    if (junctionId == -1) {
      gazebo::Junction junction;
      junction.LaneTerminations().push_back(ltExit);
      junction.LaneTerminations().push_back(ltEntry);
      junctions.push_back(junction);
    }
    else
    {
      junctions[junctionId].LaneTerminations().push_back(ltExit);
      junctions[junctionId].LaneTerminations().push_back(ltEntry);
    }
  }
}

gazebo::LaneTermination DynamicRender::CreateLaneTermination(
  const ignition::rndf::UniqueId &id) const {
    const ignition::rndf::Waypoint waypoint =
      *GetWaypointByUniqueId(id);
    const ignition::math::Pose3d pose = GetWaypointPose(
      id.X(), id.Y(), waypoint);

    double width;
    const ignition::rndf::Lane *lane = GetLaneByUniqueId(id);
    if (lane == nullptr) {
      width = Creator::DEFAULT_LANE_WIDTH;
    }
    else if (lane->Width() <= 0.0)
    {
      width = Creator::DEFAULT_LANE_WIDTH;
    }
    else
    {
      width = lane->Width();
    }

    return gazebo::LaneTermination(id, pose, width);
}

ignition::math::Vector3d
  DynamicRender::GetWaypointSphericalLocation(
    const ignition::rndf::Waypoint &wp) const {
  const ignition::math::SphericalCoordinates &location =
    wp.Location();
  return ignition::math::Vector3d(
    location.LatitudeReference().Radian(),
    location.LongitudeReference().Radian(),
    location.ElevationReference());
}

std::vector<ignition::math::Vector3d> DynamicRender::GetLanePoints(
  const ignition::rndf::Lane &lane) const {
    // Get the waypoint positions
    std::vector<ignition::math::Vector3d> waypointPositions;
    for (const auto &waypoint : lane.Waypoints()) {
      ignition::math::Vector3d location =
        GetWaypointSphericalLocation(waypoint);
      ignition::math::Vector3d position =
        projection.SphericalToGlobal(location);
      waypointPositions.push_back(position);
    }
    // Create a lane in gazebo
    return roadCreator.InterpolateRoad(waypointPositions,
        interpolationDistance);
}

void DynamicRender::GetAllWaypointLocations(
  std::vector<ignition::math::Vector3d> &positions) const {
  for (const auto &segment : rndfInfo->Segments()) {
    for (const auto &lane : segment.Lanes()) {
      for (const auto &waypoint : lane.Waypoints()) {
        positions.push_back(
          GetWaypointSphericalLocation(waypoint));
      }
    }
  }
  for (const auto &zone : rndfInfo->Zones()) {
    for (const auto &waypoint : zone.Perimeter().Points()) {
      positions.push_back(
        GetWaypointSphericalLocation(waypoint));
    }
    for (const auto &spot : zone.Spots()) {
      for (const auto &waypoint : spot.Waypoints()) {
        positions.push_back(
          GetWaypointSphericalLocation(waypoint));
      }
    }
  }
}

std::string DynamicRender::CreateWaypointName(const int segmentParentId,
  const int laneParentId,
  const int waypointId) const {
    return "waypoint_" + std::to_string(segmentParentId) +
      "_" + std::to_string(laneParentId) +
      "_" + std::to_string(waypointId);
}

}
