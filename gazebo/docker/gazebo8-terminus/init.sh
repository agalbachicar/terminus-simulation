#!/bin/bash

tmux start-server
tmux new-session -d -s gazebo
tmux send-keys 'source /home/gazebo/ws/terminus-simulation/setup.bash' C-m
tmux send-keys 'clear' C-m
tmux -2 attach-session -t gazebo